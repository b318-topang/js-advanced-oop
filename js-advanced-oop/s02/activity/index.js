/* A C T I V I T Y  O N E */

class Equipment {
	constructor(equipmentType) {
		this.equipmentType = equipmentType;
	}

	printInfo() {
		return `Info: ${this.equipmentType}`;
	}
}

/* Bulldozer */
class Bulldozer extends Equipment {
	constructor(equipmentType, model, bladeType) {
		super(equipmentType);
		this.model = model;
		this.bladeType = bladeType;
	}

	// Overriding printInfo() of class Equpiment
	printInfo() {
		return super.printInfo() + `\nThe bulldozer ${this.model} has a ${this.bladeType} blade`
	}
}

let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo());

/* Tower Crane */
class TowerCrane extends Equipment {
	constructor(equipmentType, model, hookRadius, maxCapacity) {
		super(equipmentType);
		this.model = model;
		this.hookRadius = hookRadius;
		this.maxCapacity = maxCapacity;
	}

	printInfo() {
		return super.printInfo() + `\nThe tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`;
	}
}

let towercrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500)
console.log(towercrane1.printInfo());

/* Back Loader*/
class BackLoader extends Equipment {
	constructor(equipmentType, model, type, tippingLoad) {
		super(equipmentType);
		this.model = model;
		this.type = type;
		this.tippingLoad = tippingLoad
	}

	printInfo() {
		return super.printInfo() + `\nThe loader ${this.model} is a ${this.type} loader and has a tipping load of ${this.tippingLoad} lbs`
	}
}

let backloader1 = new BackLoader("back loader", "Turle", "hydraulic", 1500)
console.log(backloader1.printInfo());

/* A C T I V I T Y  T W O */

class RegularShape {
	constructor(noSides, length) {
		this.noSides = noSides;
		this.length = length;
	}

	perimeter() {
		return "The perimeter is ";
	}

	area() {
		return "The area is ";
	}
}

/* Triangle */
class Triangle extends RegularShape {
	constructor(sides, length) {
		super();
		this.sides = sides;
		this.length = length;
	}

	getPerimeter() {
		return super.perimeter() + `${this.sides * this.length}`;
	}

	getArea() {
		const base = this.length;
		const height = (Math.sqrt(this.sides) / 2) * this.length;
		const area = (base * height) / 2;
		return super.area() + area;
	}
}

let triangle1 = new Triangle(3, 10);
console.log(triangle1.getPerimeter()); // The perimeter is 30;
console.log(triangle1.getArea()); // The area is 43.30127018922193

/* Square */
class Square extends RegularShape {
	constructor(sides, length) {
		super();
		this.sides = sides;
		this.length = length;
	}

	getPerimeter() {
		return super.perimeter() + `${this.sides * this.length}`;
	}

	getArea() {
		return super.area() + this.length ** 2;
	}
}
let square1 = new Square(4, 12);
console.log(square1.getPerimeter()); // The perimeter is 48;
console.log(square1.getArea()); // The area is 144;