/* C A P S T O N E  M A K I N G*/

/* 1. */
	class Request {
		constructor(email, content, dateRequested) {
			this.email = email;
			this.content = content;
			this.dateRequested = new Date();
		}
	}

/* 2. */
	class Person {
	  constructor() {
	    if (this.constructor === Person) {
	      throw new Error("Cannot instantiate abstract class.");
	    }
	  }

	  getFullName() {
	    throw new Error("Method 'getFullName' must be implemented.");
	  }

	  login() {
	    throw new Error("Method 'login' must be implemented.");
	  }

	  logout() {
	    throw new Error("Method 'logout' must be implemented.");
	  }
	}

/* 3. */
	class Employee extends Person {

		/* Private Fields */
		#firstName
		#lastName
		#email
		#department
		#isActive
		#requests

		constructor(firstName, lastName, email, department, isActive, requests) {
			super();
			this.#firstName = firstName;
			this.#lastName = lastName;
			this.#email = email;
			this.#department = department;
			this.#isActive = true;
			this.#requests = [];
		}

		/* Getter Methods */
		getFirstName() {
			return this.#firstName;
		}

		getLastName() {
			return this.#lastName;
		}

		getEmail() {
			return this.#email;
		}

		getDepartment() {
			return this.#department;
		}

		getIsActive() {
			return this.#isActive;
		}

		getRequests() {
			return this.#requests;
		}

		/* Setter Methods */
		setFirstName(firstName) {
			this.#firstName = firstName;
		}

		setLastName(lastName) {
			this.#lastName = lastName;
		}

		setEmail(email) {
			this.#email = email;
		}

		setDepartment(department) {
			this.#department = department;
		}

		setIsActive(isActive) {
			this.#isActive = isActive;
		}

		setRequests(requests) {
			this.#requests = requests;
		}

		/* Methods */
		addRequests(content) {
			const request = new Request(this.#email, content);
			this.#requests.push(request);
  		}
	}

	/* Test Output 1 */
	const employeeA = new Employee();
	employeeA.setFirstName("John");
	employeeA.setLastName("Smith");
	employeeA.setEmail("john@mail.com");
	employeeA.setDepartment("Marketing");
	employeeA.setIsActive(true);

	employeeA.addRequests("Vacation Leave");
	employeeA.addRequests("Vacation Leave");

	console.log(employeeA);

	/* Test Output 2 */
	const employeeB = new Employee();
	employeeB.setFirstName("Jane");
	employeeB.setLastName("Doe");
	employeeB.setEmail("jane@mail.com");
	employeeB.setDepartment("Marketing");
	employeeB.setIsActive(false);

	employeeB.addRequests("Sick Leave");
	employeeB.addRequests("Sick Leave");

	console.log(employeeB);
	
/* 4. */
	class TeamLead extends Person {

		/* Private Fields */
		#firstName
		#lastName
		#email
		#department
		#isActive
		#members

		constructor(firstName, lastName, email, department, isActive, members) {
			super();
			this.#firstName = firstName;
			this.#lastName = lastName;
			this.#email = email;
			this.#department = department;
			this.#isActive = true;
			this.#members = [];
		}

		/* Getter Methods */
		getFirstName() {
			return this.#firstName;
		}

		getLastName() {
			return this.#lastName;
		}

		getEmail() {
			return this.#email;
		}

		getDepartment() {
			return this.#department;
		}

		getIsActive() {
			return this.#isActive;
		}

		getMembers() {
			return this.#members;
		}

		/* Setter Methods */
		setFirstName(firstName) {
			this.#firstName = firstName;
		}

		setLastName(lastName) {
			this.#lastName = lastName;
		}

		setEmail(email) {
			this.#email = email;
		}

		setDepartment(department) {
			this.#department = department;
		}

		setIsActive(isActive) {
			this.#isActive = isActive;
		}

		setMembers(Members) {
			this.#members = members;
		}

		/* Methods */
		addMembers(employee) {
			if (employee.getIsActive()){
				this.#members.push(employee)
				return `Added ${employee.getFirstName()} to your team!`
			} else {
				return "You cannot add an inactive employee to your team."
			}
		}

		checkRequests(employeeEmail) {
			const employee = this.#members.find(member => member.getEmail() === employeeEmail)

			if (employee) {
				return employee.getRequests();
			} else {
				return "No matching employee found in this Team Lead's members.";
			}

		}
	}

	/* Test Output 1 */
	const teamLeadA = new TeamLead();
	teamLeadA.setFirstName("Owen");
	teamLeadA.setLastName("Orange");
	teamLeadA.setEmail("owen@mail.com");
	teamLeadA.setDepartment("Marketing");
	teamLeadA.setIsActive(true);

	console.log(teamLeadA.addMembers(employeeA));
	console.log(teamLeadA.addMembers(employeeB));

	console.log(teamLeadA.checkRequests(employeeA.getEmail())); // john@mail.com
	console.log(teamLeadA.checkRequests(employeeB.getEmail())); // jane@mail.com

	console.log(teamLeadA);

	/* Test Output 2 */
	const teamLeadB = new TeamLead();
	teamLeadB.setIsActive(false);



/* 5. */

	class Admin extends Person {

		/* Private Fields */
		#firstName
		#lastName
		#email
		#department
		#teamLeads

		constructor(firstName, lastName, email, department, teamLeads) {
			super();
			this.#firstName = firstName;
			this.#lastName = lastName;
			this.#email = email;
			this.#department = department;
			this.#teamLeads = [];
		}

		/* Getter Methods */
		getFirstName() {
			return this.#firstName;
		}

		getLastName() {
			return this.#lastName;
		}

		getEmail() {
			return this.#email;
		}

		getDepartment() {
			return this.#department;
		}

		getTeamLeads() {
			return this.#teamLeads;
		}

		/* Setter Methods */

		setFirstName(firstName) {
			this.#firstName = firstName;
		}

		setLastName(lastName) {
			this.#lastName = lastName;
		}

		setEmail(email) {
			this.#email = email;
		}

		setDepartment(department) {
			this.#department = department;
		}

		setTeamLeads(teamLeads) {
			this.#teamLeads = teamLeads;
		}

		/* Methods */
		addTeamLead(teamLead) {
			if (teamLead.getIsActive()){
				this.#teamLeads.push(teamLead)
				return `Added ${teamLead.getFirstName()} to your list of team leads!`
			} else {
				return "You cannot add an inactive team leader to your list."
			}
		}

		deactiveTeam(teamLeadEmail) {
			const teamLead = this.#teamLeads.find(lead => lead.getEmail() === teamLeadEmail);

			if (teamLead) {
				teamLead.setIsActive(false);
				teamLead.getMembers().forEach(member => {
					member.setIsActive(false);
				})
			}
		}
	}

	/* Test Output */
	const adminA = new Admin();
	adminA.setFirstName("Admin");
	adminA.setLastName("Account");
	adminA.setEmail("admin@mail.com");
	adminA.setDepartment("IT");

	console.log(adminA.addTeamLead(teamLeadA)); // Owen Orange
	console.log(adminA.addTeamLead(teamLeadB)); // Not existing

	adminA.deactiveTeam("owen@mail.com") // Deactivating the Team Owen

	console.log(adminA);


