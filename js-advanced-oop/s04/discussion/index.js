class Person {
	constructor() {
		if (this.constructor === Person) {
			throw new Error(
				"Object cannot be created from an abstract class Person"
			);
		}

		if (this.getFullName === undefined) {
			throw new Error(
				"Class must implement getFullName() method"
			);
		}
	}
}

class Employee extends Person {

	// Encapsulation, aided by private fields (#), ensures data protection.
	// Setters and getters provide controlled access to encapsulated data.

	/* private fields*/
	#firstName;
	#lastName;
	#employeeId;

	constructor(firstName, lastName, employeeId) {
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#employeeId = employeeId;
	}

	// getters methods
	getFullName() {
		return `${this.#firstName} ${this.#lastName} has employee id of ${this.#employeeId}`
	}

	getFirstName() {
		return `First Name: ${this.#firstName}`
	}

	getLastName() {
		return `Last Name: ${this.#lastName}`
	}

	getEmployeeId() {
		return this.#employeeId;
	}

	// setter methods
	setFirstName = (firstName) => {
		this.#firstName = firstName;
	}

	setLastName = (lastName) => {
		this.#lastName = lastName;
	}

	setEmployeeId = (employeeId) => {
		this.#employeeId = employeeId;
	}
}

const employeeA = new Employee('John', 'Smith', 'EM-001');

// Direct access with the field/property firstName will return undefined because the property is private
// console.log(employeeA.firstName);

// We could not directly change the value of the property firstName because the property is private
// employeeA.firstName = "David";

console.log(employeeA.getFirstName());
employeeA.setFirstName("David");
console.log(employeeA.getFirstName());

console.log(employeeA.getFullName());

const employeeB = new Employee();
console.log(employeeB.getFullName());
employeeB.setFirstName("Jill");
employeeB.setLastName("Hill");
employeeB.setEmployeeId("EM-002");
console.log(employeeB.getFullName());