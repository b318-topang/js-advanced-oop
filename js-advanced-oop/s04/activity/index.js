/* A C T I V I T Y  O N E */

class RegularShape {
	constructor() {
		if (this.constructor === RegularShape) {
			throw new Error(
				"Object cannot be created from an abstract class RegularShape"
			);
		}

		if (this.getPerimeter === undefined) {
			throw new Error(
				"Class must implement getPerimeter() method"
			);
		}

		if (this.getArea === undefined) {
			throw new Error(
				"Class must implement getArea() method"
			);
		}
	}
}

/* Square */
class Square extends RegularShape {

	// Private Fields
	#sides
	#length

	constructor(sides, length) {
		super();
		this.#sides = sides;
		this.#length = length;
	}

	// Getter Methods
	getPerimeter() {
		return "The perimeter of the square is " + `${this.#sides * this.#length}`;
	}

	getArea() {
		return "The area is " + this.#length ** 2;
	}

	getSides() {
		return this.#sides;
	}

	getLength() {
		return this.#length;
	}

	// Setter Methods
	setSides = (sides) => {
		this.#sides = sides;
	}

	setLength = (length) => {
		this.#length = length;
	}
}

// Creating a square
const square1 = new Square();
square1.setSides(4);
square1.setLength(65);

// Test output
console.log(square1.getLength()); // 65
console.log(square1.getPerimeter()); // The perimeter of the square is 260

/* Triangle */
class Triangle extends RegularShape {

	// Private Fields
	#sides
	#length

	constructor(sides, length) {
		super();
		this.#sides = sides;
		this.#length = length;
	}	

	getSides() {
		return this.#sides;
	}

	getLength() {
		return this.#length;
	}

	getPerimeter() {
		return super.perimeter() + `${this.sides * this.length}`;
	}

	getArea() {
		const base = this.#length;
		const height = (Math.sqrt(this.#sides) / 2) * this.#length;
		const area = (base * height) / 2;
		return `The area of the triangle is ${area.toFixed(3)}`;
	}

	setSides = (sides) => {
		this.#sides = sides;
	}

	setLength = (length) => {
		this.#length = length;
	}

}

// Creating a triangle
const triangle1 = new Triangle();
triangle1.setSides(3);
triangle1.setLength(15);

// Test output
console.log(triangle1.getLength()); // 15
console.log(triangle1.getArea()); // The area of the triangle is 97.428





/* A C T I V I T Y  T W O */

class User {
	constructor() {
		if (this.constructor === User) {
			throw new Error("Object cannot be created from an abstract class User");
		}
	}

	login() {
		throw new Error("Method 'login' must be implemented");
	}

	register() {
		throw new Error("Method 'register' must be implemented");
	}

	logout() {
		throw new Error("Method 'logout' must be implemented");
	}
}

class RegularUser extends User {

	// Private Fields
	#name
	#email
	#password

	constructor(name, email, password) {
		super();
		this.#name = name;
		this.#email = email;
		this.#password = password;
	}

	// Getter Methods
	login() {
		return `${this.#name} has logged in.`;
	}

	register() {
		return `${this.#name} has registered.`;
	}

	logout() {
		return `${this.#name} has logged out.`;
	}

	browseJobs() {
		return `There are 10 jobs found.`;
	}

	getName() {
		return this.#name;
	}

	getEmail() {
		return this.#email;
	}

	getPassword() {
		return this.#password;
	}

	// Setter Methods
	setName = (name) => {
		return this.#name = name;
	}

	setEmail = (email) => {
		return this.#email = email;
	}

	setPassword = (password) => {
		return this.#password = password;
	}
}

// Creating the user
const regUser1 = new RegularUser();
regUser1.setName("Dan");
regUser1.setEmail("dan@mail.com");
regUser1.setPassword("Dan12345");

// Test output
console.log(regUser1.register()); // Dan has registered
console.log(regUser1.login()); // Dan has logged in
console.log(regUser1.browseJobs()); // There are 10 jobs found
console.log(regUser1.logout()); // Dan has logged out

class Admin extends User {

	// Private Fields
	#name
	#email
	#password
	#hasAdminExpired

	constructor(name, email, password, hasAdminExpired) {
		super();
		this.#name = name;
		this.#email = email;
		this.#password = password;
		this.#hasAdminExpired = hasAdminExpired;
	}

	// Getter Methods
	login() {
		return `Admin ${this.#name} has logged in.`;
	}

	register() {
		return `Admin ${this.#name} has registered.`;
	}

	logout() {
		return `Admin ${this.#name} has logged out.`;
	}

	getName() {
		return this.#name;
	}

	getEmail() {
		return this.#email;
	}

	getPassword() {
		return this.#password;
	}
	
	getHasAdminExpired() {
		return this.#hasAdminExpired;
	}
	postJob() {
		return `Job posting added to site.`;
	}

	// Setter Methods
	setName = (name) => {
		return this.#name = name;
	}

	setEmail = (email) => {
		return this.#email = email;
	}

	setPassword = (password) => {
		return this.#password = password;
	}

	setHasAdminExpired = (hasAdminExpired) => {
		return this.#hasAdminExpired = hasAdminExpired;
	}
}

// Creating the admin
const admin = new Admin();
admin.setName("Joe");
admin.setEmail("admin_joe@mail.com");
admin.setPassword("joe12345");
admin.setHasAdminExpired(false);

// Test output
console.log(admin.register()); // Admin Joe has registered
console.log(admin.login()); // Admin Joe has logged in
console.log(admin.getHasAdminExpired()); // false
console.log(admin.postJob()); // Job added to site
console.log(admin.logout()); // Admin Joe has logged out