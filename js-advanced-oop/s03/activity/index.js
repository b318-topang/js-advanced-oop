/* A C T I V I T Y  O N E */

class RegularShape {
	constructor() {
		if (this.constructor === RegularShape) {
			throw new Error(
				"Object cannot be created from an abstract class RegularShape"
			);
		}

		if (this.getPerimeter === undefined) {
			throw new Error(
				"Class must implement getPerimeter() method"
			);
		}

		if (this.getArea === undefined) {
			throw new Error(
				"Class must implement getArea() method"
			);
		}
	}
}

/* Square */
class Square extends RegularShape {
	constructor(sides, length) {
		super();
		this.sides = sides;
		this.length = length;
	}

	getPerimeter() {
		return "The perimeter is " + `${this.sides * this.length}`;
	}

	getArea() {
		return "The area is " + this.length ** 2;
	}
}

let shape1 = new Square(4, 12);
console.log(shape1.getPerimeter()); // The perimeter is 48;
console.log(shape1.getArea()); // The area is 144;

/* A C T I V I T Y  T W O */

class Food {
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }

  getName() {
    return this.name;
  }
}

class Vegetable extends Food {
  constructor(name, breed, price) {
    super(name, price);
    this.breed = breed;
  }

  getName() {
    return `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos.`;
  }
}

const vegetable1 = new Vegetable("Pechay", "Native", 25);
console.log(vegetable1.getName());

/* A C T I V I T Y  T H R E E */

class Equipment {
	constructor(equipmentType) {
		if (this.constructor === Equipment) {
			throw new Error(
				"Object cannot be created from an abstract class Equipment"
			);
		}

		if (this.printInfo === undefined) {
			throw new Error(
				"Class must implement getPerimeter() method"
			);
		}
	}
}

/* Bulldozer */
class Bulldozer extends Equipment {
	constructor(equipmentType, model, bladeType) {
		super();
		this.equipmentType = equipmentType;
		this.model = model;
		this.bladeType = bladeType;
	}

	// Overriding printInfo() of class Equpiment
	printInfo() {
		return `Info: ${this.equipmentType}` + `\nThe bulldozer ${this.model} has a ${this.bladeType} blade`
	}
}

let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo());

/* Tower Crane */
class TowerCrane extends Equipment {
	constructor(equipmentType, model, hookRadius, maxCapacity) {
		super();
		this.equipmentType = equipmentType;
		this.model = model;
		this.hookRadius = hookRadius;
		this.maxCapacity = maxCapacity;
	}

	printInfo() {
		return `Info: ${this.equipmentType}` + `\nThe tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`;
	}
}

let towercrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500)
console.log(towercrane1.printInfo());